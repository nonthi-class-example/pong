using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    [SerializeField] private KeyCode _upKey;
    [SerializeField] private KeyCode _downKey;

    [SerializeField] private float _minY;
    [SerializeField] private float _maxY;

    [SerializeField] private float _moveSpeed;

    private Rigidbody2D _rb;

    private void Awake()
    {
        TryGetComponent(out _rb);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        float inputY = 0f;

        if (Input.GetKey(_upKey))
        {
            inputY += 1f;
        }
        if (Input.GetKey(_downKey))
        {
            inputY -= 1f;
        }

        if ((inputY > 0f && _rb.position.y >= _maxY) || (inputY < 0f && _rb.position.y <= _minY))
        {
            inputY = 0f;
        }

        _rb.velocity = inputY * _moveSpeed * Vector2.up;

        Vector2 pos = _rb.position;
        pos.y = Mathf.Clamp(pos.y, _minY, _maxY);
        _rb.position = pos;
    }
}
