using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private Ball _ballPrefab;

    [Range(0f, 180f)]
    [SerializeField] private float _spawnAngle = 90f;

    // Start is called before the first frame update
    void Start()
    {
        ThrowBall();
    }

    private void ThrowBall()
    {
        Ball ball = Instantiate(_ballPrefab, Vector3.zero, Quaternion.identity);

        float randomAngle = Random.Range(-_spawnAngle / 2, _spawnAngle / 2) * Mathf.Deg2Rad;
        Vector2 spawnDirection = new Vector2(Mathf.Cos(randomAngle), Mathf.Sin(randomAngle));
        int side = Random.Range(0, 2);
        if (side == 0)
        {
            spawnDirection = -spawnDirection;
        }

        ball.Throw(spawnDirection);
    }

    public void NotifyGoal(Player scorer)
    {
        StartCoroutine(ThrowInCoroutine());
    }

    private IEnumerator ThrowInCoroutine()
    {
        yield return new WaitForSeconds(3f);
        ThrowBall();
    }
}
