using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float _initialSpeed = 5f;
    [SerializeField] private float _maxSpeed = 20f;
    [SerializeField] private float _acceleration = 1f;

    private Rigidbody2D _rb;

    private void Awake()
    {
        TryGetComponent(out _rb);
    }

    private void FixedUpdate()
    {
        Accelerate();
    }

    void Accelerate()
    {
        float speed = _rb.velocity.magnitude;
        speed = Mathf.MoveTowards(speed, _maxSpeed, _acceleration * Time.deltaTime);
        _rb.velocity = speed * _rb.velocity.normalized;
    }

    public void Throw(Vector2 direction)
    {
        _rb.velocity = _initialSpeed * direction.normalized;
    }

    public void Goal()
    {
        Destroy(gameObject);
    }
}
