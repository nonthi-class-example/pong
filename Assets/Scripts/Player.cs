using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    private int _score;

    [SerializeField] private UnityEvent<int> _onScoreChanged;

    public void AddScore()
    {
        _score++;
        _onScoreChanged?.Invoke(_score);
    }
}
