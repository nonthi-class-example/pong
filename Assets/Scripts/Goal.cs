using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private Player _scorePlayer;

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (otherCollider.TryGetComponent(out Ball ball))
        {
            ball.Goal();
            _scorePlayer.AddScore();
            _gameController.NotifyGoal(_scorePlayer);
        }
    }
}
