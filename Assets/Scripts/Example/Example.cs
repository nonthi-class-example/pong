using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MyInt a = new MyInt(5);
        TestMethod(ref a);
        Debug.Log(a.Value);
    }

    void TestMethod(ref MyInt input)
    {
        input.Value += 3;
    }
}

public struct MyInt
{
    public int Value;

    public MyInt(int value)
    {
        Value = value;
    }
}
